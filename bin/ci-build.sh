#!/bin/bash
################################################################################
#
# Create tar file for the project and upload it to S3 repository. 
#
################################################################################

################################################################################
#
# Make sure we are in top level directory for repository.
#
dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"  # dir of this script
cd $dir
cd ..
echo "Directory:" `pwd`

################################################################################
#
# Determine project name.
#
project=`git remote -v | head -n1 | awk '{print $2}' | sed 's/.*\///' | sed 's/\.git//'`
if [ $? -ne 0 ];
then
  echo "ERROR: failed to determine project name, exiting"
  exit 1
fi
echo "Project:" $project

################################################################################
#
# Invent a unique sequential git version number.
#
d=`date +%Y%m%d`
c=`git rev-list --full-history --all --abbrev-commit | wc -l | sed -e 's/^ *//'`

ref="unknown"
old_IFS=$IFS
IFS=$'\n'
#rev=`git rev-list --full-history --all | head -1`
rev=($(cat .git/HEAD))
echo "Git Tag: " $rev
h=${rev:0:7}
echo "Commit : " $h

ref=`cat .git/FETCH_HEAD | grep $rev | awk {'{print $3}'} | tr "'" " " | tr -d " "`
echo "Branch: " $ref
IFS=$old_IFS

git_revision=${c}.${ref}.${h}.${d}
echo "Revision:" $git_revision

################################################################################
#
# Create tar file
#
tarfile=$project.$git_revision.tgz
echo "File:" $tarfile
tar czf $tarfile webroot webconf
if [ $? -ne 0 ];
then
  echo "ERROR: failed to create tar file, exiting"
  exit 1
fi

################################################################################
#
# Upload file
#
destination="s3://irismobile/application/webapp/$project/"
echo "Destination:" $destination
s3cmd --config=$HOME/.s3cfg put $tarfile $destination
if [ $? -ne 0 ];
then
  echo "ERROR: failed to upload to S3, exiting"
  exit 1
fi

################################################################################
echo "DONE"
exit 0
